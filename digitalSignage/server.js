const express = require('express'),
      expressLayouts = require('express-ejs-layouts'),
      fileUpload = require("express-fileupload"),
      Sequelize = require("sequelize"),
      models = require("./app/models"),
      app = express(),
      port = process.env.PORT || 8080; //Koristimo zadani port u env modulu ako
      // postoji,a ako ne postoji onda ce bit port 8080.


var passport = require("passport");
var flash = require("connect-flash-plus");
var morgan = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var session = require("express-session");
var router = require("./app/routes");


//moram rec gdje da gleda za staticke fajlove npr css
app.use(express.static(__dirname + '/public'));
app.use(fileUpload());
app.use(morgan("dev"));
app.use(cookieParser());
app.use(bodyParser());
app.set('view engine', 'ejs');
app.use (expressLayouts);
app.use(session({secret:"ilovenodejsilovenodejsilovenodejs", cookie:{_expires:600000}}));



//make sure this come after the express session 
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
require("./config/passport")(passport,models.user);

router(app,passport);

//models.sequelize.sync({force: true}).then(function(){
models.sequelize.sync().then(function(){
    console.log("database succesfully connected");
    var company = models.company;
    var user = models.user;

    company.findOrCreate({ where: {
      name: "Admin company",
      address: "Admin address",
      oib : "123456789",
      email: "admincompany@gmail.com"
    }}).spread((admincompany, created) => {
      console.log(admincompany.get({plain: true}));
      console.log(created);

    user.findOne({ where: {
      firstName: "admin",
      lastName: "admin",
      about: "admin",
      email: "admin@gmail.com",
      companyId: admincompany.id
    }}).then(adminUser => {

        if(!adminUser){

            user.create( {
              firstName: "admin",
              lastName: "admin",
              about: "admin",
              email: "admin@gmail.com",
              password: user.generateHash("admin1234"),
              companyId: admincompany.id
            }).then(newAdminUser => {

                console.log(newAdminUser.get({plain: true}));




            }); 


        }
        else{
          console.log(adminUser.get({plain: true}));

        }


    }); 




  });



}).catch(function(error){
    console.log("Something went wrong with Database Update! " + error);
}); 


app.listen(port, () => {
    console.log(`App listening on http:\\localhost:${port}`);
});


