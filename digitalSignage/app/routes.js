//kreiramo novi ekspres router koji ce sluziti za promjene ruta ili urlova

//create a new expres router
const express = require("express"),
    authController                     = require("./controllers/auth.controller"),
    apiController                      = require("./controllers/api.controller"),
    companyController                  = require("./controllers/company.controller"),
    userController                     = require("./controllers/user.controller"),
    videoController                    = require("./controllers/video.controller"),
    deviceController                   = require("./controllers/device.controller"),
    groupController                    = require("./controllers/group.controller"),
    playlistController                 = require("./controllers/playlist.controller")

var models = require("./models");
var company = models.company;
var user = models.user;

//export router
module.exports = function(app, passport){

    app.get("/",                                authController.login);
    app.post("/login",                          passport.authenticate("local-signin", {failureRedirect: "/"}), redirectToHomePage);
    app.get("/logout",                          isLoggedIn, authController.logout);

    app.get("/company/list",                    isLoggedIn, companyController.list);
    app.get("/company/create",                  isLoggedIn, companyController.create);
    app.post("/company/save",                   isLoggedIn, companyController.save);
    app.get("/company/show/:id",                isLoggedIn, companyController.show);
    app.get("/company/edit/:id",                isLoggedIn, companyController.edit);
    app.get("/company/delete/:id",              isLoggedIn, companyController.delete);

    app.get("/user/list",                       isLoggedIn, userController.list);
    app.get("/user/create",                     isLoggedIn, userController.create);
    app.post("/user/save",                      isLoggedIn, userController.save);
    app.get("/user/show/:id",                   isLoggedIn, userController.show);
    app.get("/user/edit/:id",                   isLoggedIn, userController.edit);
    app.get("/user/delete/:id",                 isLoggedIn, userController.delete);
    
    app.get("/video/list",                      isLoggedIn, videoController.list);
    app.get("/video/upload",                    isLoggedIn, videoController.upload);
    app.post("/video/uploadVideo",              isLoggedIn, videoController.uploadVideo);
    app.get("/video/show/:id",                  isLoggedIn, videoController.show);
    app.get("/video/delete/:id",                isLoggedIn, videoController.delete);

    app.get("/device/list",                     isLoggedIn, deviceController.list);
    app.get("/device/create",                   isLoggedIn, deviceController.create);
    app.post("/device/save",                    isLoggedIn, deviceController.save)
    app.get("/device/show/:id",                 isLoggedIn, deviceController.show);
    app.get("/device/edit/:id",                 isLoggedIn, deviceController.edit);
    app.get("/device/delete/:id",               isLoggedIn, deviceController.delete);
    app.get("/device/logs/:id/:filter",         isLoggedIn, deviceController.logs);
    app.get("/device/wifiSetup/:id",            isLoggedIn, deviceController.wifiSetup);
    app.post("/device/saveWifi",                isLoggedIn, deviceController.saveWifi);

    app.get("/group/list",                      isLoggedIn, groupController.list);
    app.get("/group/create",                    isLoggedIn, groupController.create);
    app.post("/group/save",                     isLoggedIn, groupController.save);
    app.get("/group/show/:id",                  isLoggedIn, groupController.show);
    app.get("/group/edit/:id",                  isLoggedIn, groupController.edit);
    app.get("/group/delete/:id",                isLoggedIn, groupController.delete);

    app.get("/playlist/list",                   isLoggedIn, playlistController.list);
    app.get("/playlist/create",                 isLoggedIn, playlistController.create);
    app.post("/playlist/save",                  isLoggedIn, playlistController.save);
    app.get("/playlist/show/:id",               isLoggedIn, playlistController.show);
    app.get("/playlist/edit/:id",               isLoggedIn, playlistController.edit);
    app.get("/playlist/delete/:id",             isLoggedIn, playlistController.delete);
    app.get("/playlist/publish/:id",            isLoggedIn, playlistController.publish);

    app.get("/api/getCurrentTime",              apiController.getCurrentTime);
    app.get("/api/deviceInDatabase/:mac",       apiController.deviceInDatabase);
    app.get("/api/currentPlaylist/:mac",        apiController.currentPlaylist);
    app.get("/api/wifi/:mac",                   apiController.wifi);
    app.get("/api/newLog/:mac/:logText",        apiController.newLog);




    function userAlreadyExist(value, {req}) {
        console.log("check userAlreadyExist");
        console.log(req.body);

        return new Promise((resolve, reject) => {
          user.findOne({ where: {email: req.body.email} }, function(err, u){
            if(err) {
              reject(new Error('Greška!'));
            }
            if(Boolean(u)) {
              reject(new Error('Korisnik već postoji!'))
            }
            resolve(true)
          });
        });
    }

    function redirectToHomePage(req, res){
        if (req.user.isAdmin){
          res.redirect("/company/list");
        }
        else{
          res.redirect("/video/list");
        }
    }

    function isLoggedIn(req, res, next){
        // if user is authenticated in the session, go on
        if (req.isAuthenticated())
            return next();
        else{
            res.redirect("/"); // if not redirect them to the home page
        }
    }
}