var models = require("../models");
var moment = require('moment');
const op = require("sequelize").Op;
var video = models.video;
var playlist = models.playlist;
var playlistVideo = models.playlistVideo;

module.exports = {
	//show the home page
	list: (req, res) => {
		console.log("playlist list: ");
		console.log(req.user);
		playlist.findAll({ 
			where: { companyId: { [op.eq]: req.user.companyId } },
			order: [ ['name', 'ASC'] ]
    	},
    	{ raw: true }
    	).then(allPlaylists => {

			var flashMessage;
			if( req.session.mymessage ){
				flashMessage = req.session.mymessage;
				req.session.mymessage = null;
			}
			res.render('playlist/list', {
				user: req.user,
				allPlaylists: allPlaylists,
				message: flashMessage,
				moment: moment
			});

		}).catch(error => {
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći popise!"};
			res.redirect('/playlist/list');
		});
	},

	show: (req, res) => {
		console.log("inside show!");
		console.log(req.params);
		playlist.findById(req.params.id, {
			include: [{ model: video, as: 'videos' }]
		}).then(pl => {
			console.log(pl.get({plain: true}));
			res.render('playlist/show', {
				user: req.user,
				moment: moment,
				rootURL: req.protocol + '://' + req.get('host') + "/videos/" + req.user.companyId + "/",
				pl: pl
			});
		}).catch(error => {
			console.log("Nije moguće pronaći popis!");
			console.log(error);
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći popis!"};
			res.redirect('/playlist/list');
		});
	},

	edit: (req, res) => {
		console.log("inside edit!");
		console.log(req.params);
		playlist.findById(req.params.id, {
			include: [{ model: video, as: 'videos' }]
		}).then(pl => {

			video.findAll({ 
				where: { companyId: { [op.eq]: req.user.companyId } }, 
				order: [['name', 'ASC']]
			}, 
			{ raw: true }
			).then(allVideos => {

				playlistVideo.findAll({
					where: { playlistId: { [op.eq]: pl.id } },
				  	include: [ {model: video, as: 'videoItem'} ],
				  	order: [['position', 'ASC']]
				},
				{ raw: true }
				).then(allPlItems => {

					var itemsHidden = "";
					allPlItems.forEach(function(item, index) {
						itemsHidden += ("added-" + item.position + "-video-" + item.videoId + ",");
					}); 
					res.render('playlist/edit', {
						user: req.user,
						moment: moment,
						allPlItems: allPlItems,
						itemsHidden: itemsHidden,
						allVideos: allVideos,
						pl: pl
					});
				}).catch(error => {
					console.log("Nije moguće pronaći allPlItems!");
					console.log(error);
					req.session.mymessage = {type: "danger", text: "Nije moguće pronaći videa iz popisa!"};
					res.redirect('/playlist/list');
				});

			}).catch(error => {
				console.log("Nije moguće pronaći videa!");
				console.log(error);
				req.session.mymessage = {type: "danger", text: "Nije moguće pronaći videa!"};
				res.redirect('/playlist/list');
			});	
			
		}).catch(error => {
			console.log("Nije moguće pronaći popis!");
			console.log(error);
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći popis!"};
			res.redirect('/playlist/list');
		});
	},

	create: (req, res) => {
		video.findAll({ 
			where: { companyId: { [op.eq]: req.user.companyId}}, 
			order: [['name', 'ASC']] }, 
		{ raw: true }
		).then(allVideos => {
			res.render('playlist/create', {
				user: req.user,
				allVideos: allVideos
			});
		}).catch(error => {
			console.log("Nije moguće pronaći videa!");
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći videa!"};
			res.redirect('/playlist/list');
		});	
	},

	save: (req, res) => {
		console.log("playlist save body params:");
		console.log(req.body);
		var plItems = req.body.items.split(',');
		var plItemsArray = [];
		

		if(req.body.id){

			playlist.update({  
				name: req.body.name,
	        	description: req.body.description,
	        	startTime: moment(req.body.startTime, "DD-MM-YYYY HH:mm")
			},
			{ where: { id: { [op.eq]: req.body.id } }}
			).then(() => {

				playlistVideo.destroy({
					where: { playlistId: { [op.eq]: req.body.id } },
				}).then(function () {

					plItems.forEach(item => {
						if(item != ""){
							itemInfos = item.split('-');
							plItemsArray.push({videoId: itemInfos[3], position: itemInfos[1], playlistId: req.body.id});
						}
					});

					if(plItems){
						playlistVideo.bulkCreate(plItemsArray).catch(error => {
							console.log("dogodila se greška pri kreiranju stavki popisa!");
							console.log(error);
							req.session.mymessage = {type: "danger", text: "Dogodila se greška pri kreiranju stavki popisa!"};
							res.redirect('/playlist/list');
						});
					}

					req.session.mymessage = {type: "success", text: "Popis ažuriran!"};
					res.redirect('/playlist/list');
				});

			}).catch(error => {
				console.log("dogodila se greška pri ažuriranju!");
				req.session.mymessage = {type: "danger", text: "Dogodila se greška pri ažuriranju!"};
				res.redirect('/playlist/list');
			});


		}
		else{

			playlist.create({  
				companyId: req.user.companyId,
				name: req.body.name,
		        description: req.body.description,
		        startTime: moment(req.body.startTime, "DD-MM-YYYY HH:mm")
			}).then(newPl => {		

				plItems.forEach(item => {
					if(item != ""){
						itemInfos = item.split('-');
						plItemsArray.push({videoId: itemInfos[3], position: itemInfos[1], playlistId: newPl.id});
					}
				});
		    	if(plItems){
					playlistVideo.bulkCreate(plItemsArray).catch(error => {
						console.log("dogodila se greška pri kreiranju stavki popisa!");
						console.log(error);
						req.session.mymessage = {type: "danger", text: "Dogodila se greška pri kreiranju stavki popisa!"};
						res.redirect('/playlist/list');
					});
				}

	            req.session.mymessage = {type: "success", text: "Popis kreiran!"};
				res.redirect('/playlist/list');
				
			}).catch(error => {
				console.log("dogodila se greška pri kreiranju popisa!");
				console.log(error);
				req.session.mymessage = {type: "danger", text: "Dogodila se greška pri kreiranju popisa!"};
				res.redirect('/playlist/list');
			});
		}

		
	},

	delete: (req, res) => {
		console.log("inside delete!");
		console.log(req.params);

		playlist.destroy({ 
			where: { id: { [op.eq]: req.params.id } } 
		}).then(() => {
			req.session.mymessage = {type: "success", text: "Popis uklonjen!"};
			res.redirect('/playlist/list');
		}).catch(error => {
			req.session.mymessage = {type: "danger", text: "Nije moguće ukloniti popis!"};
			res.redirect('/playlist/list');
		});
	},

	publish: (req, res) => {
		console.log("inside publish!");
		console.log(req.params);
		var currDateTime = moment().format("DD-MM-YYYY HH:mm").toString();
		console.log(currDateTime); 
		playlist.update(
			{ startTime: moment(currDateTime, "DD-MM-YYYY HH:mm") },
			{ where: { id: { [op.eq]: req.params.id } }}
		).then(rows => {
			console.log("update then"); 
			console.log(rows);
			req.session.mymessage = {type: "success", text: "Popis poslan!"};
			res.redirect('/playlist/list');
		}).catch(error => {
			console.log("error");
			console.log(error);
			req.session.mymessage = {type: "danger", text: "Nije moguće ažurirati popis!"};
			res.redirect('/playlist/list');
		});
	},

	

};