var models = require("../models");
var moment = require('moment');
const op = require("sequelize").Op;
var device = models.device;
var playlist = models.playlist;
var group = models.group;
var wifisetup = models.wifisetup;
var log = models.log;

module.exports = {
	//show the home page
	list: (req, res) => {
		console.log("video list: ");
		console.log(req.user);
		device.findAll({ 
			where:{ companyId: { [op.eq]: req.user.companyId} },
			order: [ ['name', 'ASC'] ]
    	},
    	{ raw: true }
    	).then(allDevices => {

			var flashMessage;
			if( req.session.mymessage ){
				flashMessage = req.session.mymessage;
				req.session.mymessage = null;
			}
			res.render('device/list', {
				user: req.user,
				allDevices: allDevices,
				message: flashMessage
			});
		}).catch(error => {
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći uređaje!"};
			res.redirect('/device/list');
		});
	},

	show: (req, res) => {
		console.log("inside show!");
		console.log(req.params);
		device.findById(req.params.id, {
			include: [{ model: group, as: 'group' }]
		}).then(dev => {
			console.log(dev.get({plain: true}));
			
			res.render('device/show', {
				user: req.user,
				moment: moment,
				dev: dev
			});

		}).catch(error => {
			console.log("Nije moguće pronaći uređaj!");
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći uređaj!"};
			res.redirect('/device/list');
		});
	},

	edit: (req, res) => {
		console.log("inside edit!");
		console.log(req.params);
		device.findById(req.params.id).then(dev => {
			console.log(dev.get({plain: true}));
			group.findAll({ 
				where:{ companyId: { [op.eq]: req.user.companyId}}, order: [['name', 'ASC']]}, 
				{ raw: true })
			.then(allGroups => {
				res.render('device/edit', {
					user: req.user,
					dev: dev,
					allGroups: allGroups
				});
			}).catch(error => {
				console.log("Nije moguće pronaći grupe!");
				req.session.mymessage = {type: "danger", text: "Nije moguće pronaći grupe!"};
				res.redirect('/device/list');
			});	
		}).catch(error => {
			console.log("Nije moguće pronaći uređaj!");
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći uređaj!"};
			res.redirect('/device/list');
		});
	},

	create: (req, res) => {
		group.findAll({ 
			where: { companyId: { [op.eq]: req.user.companyId } }, 
			order: [['name', 'ASC']]}, 
			{ raw: true })
		.then(allGroups => {
			res.render('device/create', {
				user: req.user,
				allGroups: allGroups
			});
		}).catch(error => {
			console.log("Nije moguće pronaći grupe!");
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći grupe!"};
			res.redirect('/device/list');
		});	
	},

	save: (req, res) => {
		console.log("device save body params:");
		console.log(req.body);

		if(req.body.id){
			device.update({  
				name: req.body.name,
		        macaddress: req.body.macaddress,
		        place: req.body.place,
		        description: req.body.description,
		        groupId: req.body.groupId
			},
			{ where: { id: req.body.id } }
			).then(() => {
				req.session.mymessage = {type: "success", text: "Uređaj ažuriran!"};
				res.redirect('/device/list');
			}).catch(error => {
				console.log("dogodila se greška pri ažuriranju!");
				req.session.mymessage = {type: "danger", text: "Dogodila se greška pri ažuriranju!"};
				res.redirect('/device/list');
			});


		}else{

			device.findOne({ where: {macaddress: req.body.macaddress } }).then(existingDev => {
				if(existingDev){
					req.session.mymessage = {type: "danger", text: "Uređaj s tom mac adresom već postoji!"};
					res.redirect('/device/list');
				}else{

					device.create({  
						name: req.body.name,
				        macaddress: req.body.macaddress,
				        place: req.body.place,
				        description: req.body.description,
				        companyId: req.user.companyId,
				        groupId: req.body.groupId
					}).then(newDev => {		
						console.log(newDev.get({plain: true}));
			            console.log("uređaj kreiran!");
			            req.session.mymessage = {type: "success", text: "Uređaj kreiran!"};
						res.redirect('/device/list');
					}).catch(error => {
						console.log("dogodila se greška pri kreiranju!");
						req.session.mymessage = {type: "danger", text: "Dogodila se greška pri kreiranju!"};
						res.redirect('/device/list');
					});
				}
			})
		}
	},

	delete: (req, res) => {
		console.log("inside delete!");
		console.log(req.params);

		//TODO DELETE DEVICE LOGS 
		device.destroy({ 
			where: { id: { [op.eq]: req.params.id } } 
		}).then(() => {
			req.session.mymessage = {type: "success", text: "Uređaj uklonjen!"};
			res.redirect('/device/list');
		}).catch(error => {
			req.session.mymessage = {type: "danger", text: "Nije moguće ukloniti uređaj!"};
			res.redirect('/device/list');
		});
	},

	logs: (req, res) => {
		console.log("inside logs");
		var logfilter = false;
		if(req.params.filter == "1")
			logfilter = true;
		console.log("logfilter is: ");
		console.log(logfilter);

		if(logfilter == true){
			log.findAll({ 
				where:{ 
					deviceId: { [op.eq]: req.params.id}, 
					filterflag: { [op.eq]: true } 
				},
				order: [ ['id', 'DESC']]
	    	},
	    	{ raw: true }
	    	).then(allLogs => {
	    		res.render('device/logs', {
					user: req.user,
					deviceId: req.params.id,
					logfilter: logfilter,
					allLogs: allLogs,
					moment: moment
				});
	    	});
		}else{
			log.findAll({ 
				where:{ 
					deviceId: { [op.eq]: req.params.id}
				},
				order: [ ['id', 'DESC']]
	    	},
	    	{ raw: true }
	    	).then(allLogs => {
	    		res.render('device/logs', {
					user: req.user,
					deviceId: req.params.id,
					logfilter: logfilter,
					allLogs: allLogs,
					moment: moment
				});
	    	});
		}
		
		
	},

	wifiSetup: (req, res) => {
		console.log("inside wifi");
		device.findById(req.params.id).then(dev => {
			res.render("device/wifiSetup", {dev: dev, user: req.user});
		}).catch(error => {
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći uređaj!"};
			res.redirect('/device/list');
		});
		
	},

	saveWifi: (req, res) => {
		console.log("inside saveWifi");
		device.findById(req.body.id).then(dev => {

			wifisetup.findOne({ where: {
		      deviceId: {[op.eq]: req.body.id}
		    }}).then(deviceWifi => {
		    	if(!deviceWifi){
		    		//device wifi ne postoji, kreiraj
		    		wifisetup.create({  
						deviceId: req.body.id,
				        name: req.body.name,
				        password: req.body.password,
				        type: req.body.type
					}).then(newDeviceWifi => {		
						console.log(newDeviceWifi.get({plain: true}));
			            console.log("wifi postavke kreirane!");
			            req.session.mymessage = {type: "success", text: "Wifi postavke kreirane!"};
						res.redirect('/device/list');
					}).catch(error => {
						console.log("dogodila se greška pri kreiranju!");
						req.session.mymessage = {type: "danger", text: "Dogodila se greška pri kreiranju!"};
						res.redirect('/device/list');
					});

		    	}else{

		    		deviceWifi.update({
				        name: req.body.name,
				        password: req.body.password,
				        type: req.body.type
					}).then(updatedDevWifi => {
						console.log("Wifi postavke ažurirane!");
						console.log(updatedDevWifi.get({plain: true}));
						req.session.mymessage = {type: "success", text: "Wifi postavke ažurirane!"};
						res.redirect('/device/list');
					}).catch(error => {
						console.log("dogodila se greška pri ažuriranju!");
						req.session.mymessage = {type: "danger", text: "Dogodila se greška pri ažuriranju!"};
						res.redirect('/device/list');
					});

		    	}

		    });

			
		}).catch(error => {
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći uređaj!"};
			res.redirect('/device/list');
		});

	}

};