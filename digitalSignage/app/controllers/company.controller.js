var models = require("../models");
var fs = require('fs');
const op = require("sequelize").Op;
var company = models.company;
var user = models.user;
const VIDEOSDIR = "C:\\Users\\asus\\Documents\\zavrsni-nodejs\\digitalSignage\\public\\videos\\";


module.exports = {

	list: (req, res) => {
		console.log("company list: ");
		company.findAll({ 
			where:{
				name: { [op.ne]: "admin company"},
				email: {[op.or]: {
			      [op.ne]: "admincompany@gmail.com",
			      [op.eq]: null
			    }}
			},
			order: [
			    ['name', 'ASC']
    		]
    	},
    	{
    		raw: true
		}).then(allCompanies => {
			var flashMessage;
			if( req.session.mymessage ){
				flashMessage = req.session.mymessage;
				req.session.mymessage = null;
			}

			res.render('company/list', {
				user: req.user,
				allCompanies: allCompanies,
				message: flashMessage
			});
		}).catch(error => {
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći kompanije!"};
			res.redirect('/company/list');
		});
	},

	show: (req, res) => {
		console.log("inside show!");
		console.log(req.params);
		company.findById(req.params.id).then(comp => {
			console.log(comp.get({plain: true}));
			res.render('company/show', {
				user: req.user,
				comp: comp
			});
		}).catch(error => {
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći kompaniju!"};
			res.redirect('/company/list');
		});
		
	},

	edit: (req, res) => {
		company.findById(req.params.id).then(comp => {
			res.render('company/edit', {
				user: req.user,
				comp: comp
			});
		}).catch(error => {
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći kompaniju!"};
			res.redirect('/company/list');
		});
		
	},

	delete: (req, res) => {
		console.log("inside delete!");
		console.log(req.params);

		/*TODO delete all videos, groups, devices, playlists and video files on disk */

		company.destroy({ 
			where: { id: { [op.eq]: req.params.id } } 
		}).then(() => {
			req.session.mymessage = {type: "success", text: "Kompanija uklonjena!"};
			res.redirect('/company/list');
		}).catch(error => {
			req.session.mymessage = {type: "danger", text: "Nije moguće ukloniti kompaniju!"};
			res.redirect('/company/list');
		});
		
	},

	create: (req, res) => {
		console.log("company create:");
		res.render('company/create', {
			user: req.user
		});
	},

	save: (req, res) => {
		console.log("company save body params:");
		console.log(req.body);

		if(req.body.id){

			comp.update(
			{  
				name: req.body.name,
		        address: req.body.address,
		        oib: req.body.oib,
		        phone: req.body.phone ? req.body.phone : null,
		        email: req.body.email ? req.body.email : null
			},
			{ where: { id: { [op.eq]: req.body.id }  }}
			).then(() => {
				console.log("kompanija ažurirana!");
				console.log(updatedComp.get({plain: true}));

				req.session.mymessage = {type: "success", text: "Kompanija ažurirana!"};
				res.redirect('/company/list');

			}).catch(error => {
				console.log("dogodila se greška pri ažuriranju!");
				req.session.mymessage = {type: "danger", text: "Dogodila se greška pri ažuriranju!"};
				res.redirect('/company/list');
			});

		}else{

			company.findOne({ 
				where: { name: { [op.eq]: req.body.name } } 
			}).then(existingCompany => {
				if(existingCompany){
					req.session.mymessage = {type: "danger", text: "Kompanija već postoji!"};
					res.redirect('/company/list');
				}else{

					company.create({  
						name: req.body.name,
				        address: req.body.address,
				        oib: req.body.oib,
				        phone: req.body.phone ? req.body.phone : null,
				        email: req.body.email ? req.body.email : null
					}).then(newCompany => {		
						console.log(newCompany.get({plain: true}));
			            console.log("kompanija kreirana!");
			            var companyDir = VIDEOSDIR + newCompany.id.toString();
			            console.log("companyDir: " + companyDir);
			            if (!fs.existsSync(companyDir)){
						    fs.mkdirSync(companyDir);
						}
			            req.session.mymessage = {type: "success", text: "Kompanija kreirana!"};
						res.redirect('/company/list');
					}).catch(error => {
						console.log("dogodila se greška pri kreiranju!");
						req.session.mymessage = {type: "danger", text: "Dogodila se greška pri kreiranju!"};
						res.redirect('/company/list');
					});
				}

			})

		

		

		}
	}

};