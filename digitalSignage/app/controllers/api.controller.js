var models = require("../models");
var moment = require('moment');
const op = require("sequelize").Op;
const VIDEOSDIR = "C:\\Users\\asus\\Documents\\zavrsni-nodejs\\digitalSignage\\public\\videos\\";
const VIDEOSURL = "http://anadigitalsignage.com:8080/videos/"
var device = models.device;
var playlist = models.playlist;
var video = models.video;
var group = models.group;
var wifisetup = models.wifisetup;
var playlistVideo = models.playlistVideo;
var log = models.log;

module.exports = {

    getCurrentTime: (req, res) => {
        console.log("getTime:");
        var currentTime = moment(new Date()).format("YYYY-MM-DD HH:mm")
        var infos = {status: 0, message: "ok", currentTime: currentTime };
        res.send(infos);
    },

    deviceInDatabase: (req, res) => {
        console.log("deviceInDatabase");
        console.log(req.params);
        device.findOne({ 
            where: { macaddress: { [op.eq]: req.params.mac } } 
        }).then(dev => {

            var deviceFound = true; //true or false
            if(dev == null)
                deviceFound = false;

            var infos = {status: 0, message: "ok", deviceFound: deviceFound };
            res.send(infos);
            
        }).catch(error => {
            var infos = {status: 1, message: "error", deviceFound: false };
            res.send(infos);
        }); 
    },

    currentPlaylist: (req, res) => {

        console.log("currentPlaylist");
        console.log(req.params);
        device.findOne({ 
            where: { macaddress: { [op.eq]: req.params.mac }},
            include: [{ 
                model: group, as: 'group', 
                include: [{ 
                    model: playlist, 
                    as: 'playlists', 
                    include: [{ 
                        model: video, 
                        as: 'videos' 
                    }] 
                }] 
            }],
            order: [
                [ models.group, 'id', 'ASC' ],
                [ models.group, models.playlist, 'startTime', 'DESC' ] 
            ]
        }).then(dev => {

            if(dev != null && dev.groupId != null){

                if(dev.group.playlists.length > 0){
                    console.log(dev.group.playlists.length);
                    console.log(dev.group.playlists[0].name + " " + dev.group.playlists[0].startTime);

                    var currPl = dev.group.playlists[0];

                    playlistVideo.findAll({ 
                        where: { playlistId: { [op.eq]: currPl.id }},
                        order: [['position', 'ASC']],
                        include: [{ 
                            model: video, 
                            as: 'videoItem'
                        }]
                    }).then(allPlaylistVideos => {

                        var videoInfos = [];
                        allPlaylistVideos.forEach(function(vid, index) {

                            console.log("currPl.videos");
                            console.log(vid.position);
                            console.log(vid.videoItem.path);

                            var videoItem = {
                                name: vid.videoItem.name, 
                                duration: vid.videoItem.duration,
                                videoURL: vid.videoItem.path.replace(VIDEOSDIR, VIDEOSURL).replace("\\", "/")
                            };
                            videoInfos.push(videoItem);
                        }); 

                        var infos = {
                            status: 0, 
                            message: "ok",
                            playlistID: currPl.id,
                            playlistName: currPl.name,
                            updatedAt: moment(currPl.updatedAt).format("YYYY-MM-DD HH:mm"),
                            startTime: moment(currPl.startTime).format("YYYY-MM-DD HH:mm"),
                            totalVideos: allPlaylistVideos.length,
                            videos: videoInfos
                        };
                        res.send(infos);

                        });

                    

                }else{
                    var infos = {status: 1, message: "no playlists in group" };
                    res.send(infos);
                }

            }else{
                var infos = {status: 1, message: "device not found or not in group" };
                res.send(infos);
            }
            
        }).catch(error => {
            console.log(error);
            var infos = {status: 1, message: "error" };
            res.send(infos);
        }); 

    },

    wifi: (req, res) => {
        console.log("getWifi");
        console.log(req.params);
        device.findOne({ 
            where: { macaddress: { [op.eq]: req.params.mac } } 
        }).then(dev => {

            if(dev != null){

                wifisetup.findOne({ 
                    where: { deviceId: { [op.eq]: dev.id } } 
                }).then(wfs => {

                    if(wfs != null){

                        var infos = {
                            status: 0, 
                            message: "ok",
                            networkName: wfs.name,
                            networkType: wfs.type,
                            networkPassword: wfs.password
                        };

                        wfs.destroy().then(() => {
                            res.send(infos);
                        })
                        
                    }else{

                        var infos = {status: 1, message: "wifi setup not found"};
                        res.send(infos);
                    }
                    
                }).catch(error => {
                    console.log(error);
                    var infos = {status: 1, message: "error wifisetup"};
                    res.send(infos);
                }); 

            }else{
                var infos = {status: 1, message: "device not found" };
                res.send(infos);
            }
            
        }).catch(error => {
            var infos = {status: 1, message: "error device"};
            res.send(infos);
        }); 
    },

    newLog: (req, res) => {
        console.log("newLog");
        console.log(req.params);
        device.findOne({ 
            where: { macaddress: { [op.eq]: req.params.mac }}
        }).then(dev => {
            console.log("dev found");
            var logText = req.params.logText;
            var logFilterFlag = true;
            if(logText.startsWith("Alive")){ //true ili false
                logFilterFlag = false;
            }
            console.log(logText);
            console.log(logFilterFlag);

            log.create({  
                deviceId: dev.id,
                filterflag: logFilterFlag,
                logstring: logText
            }).then(newLog => { 
                console.log("then");
                var infos = {status: 0, message: "ok"};
                res.send(infos);
            }).catch(error => {
                console.log("catch");
                console.log(error);
                var infos = {status: 1, message: "error on creating log"};
                res.send(infos);
            });     


        }).catch(error => {
            var infos = {status: 1, message: "error device"};
            res.send(infos);
        }); 


    }




};