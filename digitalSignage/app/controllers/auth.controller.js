module.exports = {

    login:(req,res) => {
        res.render("auth/login",{message: req.flash("loginMessage"),login:{message: "login"}});
    },

    logout: (req,res) => {
       req.logout();
       res.redirect("/");
    } 
};