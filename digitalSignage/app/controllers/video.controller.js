var path = require('path');
const VIDEOSDIR = "C:\\Users\\asus\\Documents\\zavrsni-nodejs\\digitalSignage\\public\\videos\\";
const FFPROBEPATH = "C:/Users/asus/Documents/ffmpeg/ffprobe.exe";
var ffmpeg = require('fluent-ffmpeg');
ffmpeg.setFfprobePath(FFPROBEPATH);
var fs = require('fs');
var models = require("../models");
var video = models.video;
const op = require("sequelize").Op;

module.exports = {

    //show the home page
    list: (req, res) => {
        console.log("video list: ");
        console.log(req.user);
        video.findAll({ 
            where:{ companyId: { [op.eq]: req.user.companyId} },
            order: [['name', 'ASC']]
        },
        { raw: true }
        ).then(allVideos => {
            var flashMessage;
            if( req.session.mymessage ){
                flashMessage = req.session.mymessage;
                req.session.mymessage = null;
            }

            res.render('video/list', {
                user: req.user,
                allVideos: allVideos,
                message: flashMessage,
                rootURL: req.protocol + '://' + req.get('host') + "/videos/" + req.user.companyId + "/"
                //http://localhost:8080/videos/2/
            });
        }).catch(error => {
            req.session.mymessage = {type: "danger", text: "Nije moguće pronaći videa!"};
            res.redirect('/video/list');
        });
    },

    show: (req, res) => {

        console.log("inside show!");
        console.log(req.params);
        video.findById(req.params.id).then(vid => {
            console.log(vid.get({plain: true}));
            res.render('video/show', {
                user: req.user,
                vid: vid,
                rootURL: req.protocol + '://' + req.get('host') + "/videos/" + req.user.companyId + "/"
                ////http://localhost:8080/videos/2/
            });
        })
    },

    upload: (req, res) => {
        console.log("Inside upload!");
        res.render('video/upload', {
            user:req.user
        });
    },

    uploadVideo: (req, res) => {
        console.log("Inside uploadVideo!");
        console.log(req.body);
        if (!req.files)
            return res.status(400).send('No files were uploaded!');
         
        // The name of the input field (i.e. "videoFile") is used to retrieve the uploaded file
        var videoFile = req.files.video;
        console.log("uploaded file: ");
        console.log(videoFile);

        var videoPath = VIDEOSDIR + req.user.companyId.toString() + "\\" + videoFile.name;
        console.log("video will be uploaded here:" + videoPath);
        // Use the mv() method to place the file somewhere on your server
        videoFile.mv(videoPath, function(err) {
            if (err){
                console.log("an error occurred on video upload!");
                return res.status(500).send(err);
            }else{
                console.log("video uploaded successfully!");
                
                ffmpeg.ffprobe(videoPath, function(err, metadata) {
                    if (err) {
                        console.error(err);
                    } else {

                        console.log("metadata.streams");
                        console.log(metadata.streams);
                        // metadata should contain 'width', 'height' and 'display_aspect_ratio'
                        var videoStreamInfos = metadata.streams.filter(obj => { return obj.codec_name == 'h264' })[0];
                        console.log("videoStreamInfos");
                        console.log(videoStreamInfos);
                        var videoFormatInfos = metadata.format;
                        var videoSize = fs.statSync(videoPath)["size"] / 1000000.0;

                        video.create({
                            name: videoFile.name,
                            duration: parseInt(videoFormatInfos.duration),
                            path: videoPath,
                            format: videoStreamInfos.width + 'x' + videoStreamInfos.height,
                            size: videoSize.toFixed(2),
                            companyId: req.user.companyId
                        }).then(newVideo => {
                            req.session.mymessage = {type: "success", text: "Video/a uspješno dodan(a)!"};
                            console.log(newVideo.get({plain: true}));
                            console.log("newVideo kreirana!");

                        }).catch(error => {
                            console.log("dogodila se greška pri dodavanju videa!");
                            req.session.mymessage = {type: "danger", text: "Dogodila se greška pri dodavanju videa!"};
                        });

                    }
                });

                return res.status(200).send("OK!");
            }
        });
    },

    delete: (req, res) => {
        console.log("inside delete!");
        console.log(req.params);
        video.findById(req.params.id).then(vid => {
            var videoPath = vid.path;
            vid.destroy().then(() => {
                //obrisati video fajl 
                fs.unlinkSync(videoPath);
                req.session.mymessage = {type: "success", text: "Video uklonjen!"};
                res.redirect('/video/list');
            });
        }).catch(error => {
            req.session.mymessage = {type: "danger", text: "Nije moguće pronaći video!"};
            res.redirect('/video/list');
        });

        
    }
};