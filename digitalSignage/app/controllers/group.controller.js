var models = require("../models");
const op = require("sequelize").Op;
var moment = require('moment');
var group = models.group;
var playlist = models.playlist;
var device = models.device;
var playlistGroup = models.playlistGroup;

module.exports = {

	list: (req, res) => {
		console.log("group list: ");
		group.findAll({ 
			where:{ companyId: { [op.eq]: req.user.companyId } },
			order: [ ['name', 'ASC'] ]
    	},
    	{ raw: true }
    	).then(allGroups => {
			var flashMessage;
			if( req.session.mymessage ){
				flashMessage = req.session.mymessage;
				req.session.mymessage = null;
			}

			res.render('group/list', {
				user: req.user,
				allGroups: allGroups,
				message: flashMessage
			});
		}).catch(error => {
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći grupe!"};
			res.redirect('/group/list');
		});

	},

	show: (req, res) => {
		console.log("inside show!");
		console.log(req.params);
		group.findById(req.params.id, {
			include: [{ model: playlist, as: 'playlists' }]
		}).then(gr => {
			console.log(gr.get({plain: true}));
			device.findAll({ 
				where: { groupId: { [op.eq]: gr.id } },
				order: [ ['name', 'ASC'] ]
	    	},
	    	{ raw: true }
	    	).then(allDevices => {
	    		res.render('group/show', {
					user: req.user,
					allDevices: allDevices,
					gr: gr
				});
	    	}).catch(error => {
				req.session.mymessage = {type: "danger", text: "Nije moguće pronaći uređaje!"};
				res.redirect('/group/list');
			});
			
		}).catch(error => {
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći grupu!"};
			res.redirect('/group/list');
		});
		
	},

	edit: (req, res) => {
		console.log("inside edit!");
		console.log(req.params);
		group.findById(req.params.id, {
			include: [{ model: playlist, as: 'playlists' }]
		}).then(gr => {
			console.log(gr.get({plain: true}));

			device.findAll({ 
				where:{ companyId: { [op.eq]: req.user.companyId } },
				order: [ ['name', 'ASC'] ]
	    	},
	    	{ raw: true }
	    	).then(allDevices => {

				playlist.findAll({ 
					where:{ companyId: { [op.eq]: req.user.companyId } },
					order: [ ['name', 'ASC'] ]
			    },
			    { raw: true }
		    	).then(allPlaylists => {

					device.findAll({ 
						where: { groupId: { [op.eq]: gr.id } },
						order: [ ['name', 'ASC'] ]
			    	},
			    	{ raw: true }
			    	).then(allGroupDevices => {
			    		res.render('group/edit', {
							user: req.user,
							allDevices: allDevices,
							allGroupDevices: allGroupDevices,
							allPlaylists: allPlaylists,
							moment: moment,
							gr: gr
						});
			    	}).catch(error => {
						req.session.mymessage = {type: "danger", text: "Nije moguće pronaći uređaje!"};
						res.redirect('/group/list');
					});

				}).catch(error => {
					console.log(error);
					req.session.mymessage = {type: "danger", text: "Nije moguće pronaći popise!"};
					res.redirect('/group/list');
				});

			}).catch(error => {
				req.session.mymessage = {type: "danger", text: "Nije moguće pronaći uređaje!"};
				res.redirect('/group/list');
			});

		}).catch(error => {
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći grupu!"};
			res.redirect('/group/list');
		});
		
	},

	delete: (req, res) => {
		console.log(req.params);
		device.update(
			{ groupId: null },
			{ where: { groupId: { [op.eq]: req.params.id } } 
		}).then(success => {

			group.destroy({ 
				where: { id: { [op.eq]: req.params.id } }
			}).then(() => {
				req.session.mymessage = {type: "success", text: "Grupa uklonjena!"};
				res.redirect('/group/list');
			}).catch(error => {
				console.log("dogodila se greška pri brisanju grupe!");
				console.log(error);
				req.session.mymessage = {type: "danger", text: "Dogodila se greška pri brisanju grupe!"};
				res.redirect('/group/list');
			});
			
		}).catch(error => {
			console.log("dogodila se greška pri brisanju uređaja iz grupe!");
			console.log(error);
			req.session.mymessage = {type: "danger", text: "Dogodila se greška pri brisanju uređaja iz grupe!"};
			res.redirect('/group/list');
		});
		
	},

	create: (req, res) => {
		device.findAll({ 
			where:{ companyId: { [op.eq]: req.user.companyId } },
			order: [ ['name', 'ASC'] ]
    	},
    	{ raw: true }
    	).then(allDevices => {

			playlist.findAll({ 
				where:{ companyId: { [op.eq]: req.user.companyId } },
				order: [ ['name', 'ASC'] ]
		    },
		    { raw: true }
	    	).then(allPlaylists => {
				
				res.render('group/create', {
					user: req.user,
					allPlaylists: allPlaylists,
					allDevices: allDevices,
					moment: moment
				});

			}).catch(error => {
				console.log(error);
				req.session.mymessage = {type: "danger", text: "Nije moguće pronaći popise!"};
				res.redirect('/group/list');
			});

		}).catch(error => {
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći uređaje!"};
			res.redirect('/group/list');
		});

	},

	save: (req, res) => {
		console.log("group save body params:");
		console.log(req.body);

		var newGroupDevices = [];
		var newGroupPlaylists = [];
		
		if(req.body.id){
			console.log("UPDATE!");

			for (var key in req.body) {
		    	if(key.startsWith("dev-") && req.body[key] == "on"){
		    		var devId = key.split('-')[1];
		    		newGroupDevices.push(devId);
		    	}else if(key.startsWith("pl-") && req.body[key] == "on"){
		    		var plId = key.split('-')[1];
		    		newGroupPlaylists.push({groupId: parseInt(req.body.id), playlistId: parseInt(plId) });
		    	}
		    }
		    console.log("newGroupDevices");
		    console.log(newGroupDevices);
		    console.log("newGroupPlaylists");
		    console.log(newGroupPlaylists);

			group.update({
				name: req.body.name,
		        description: req.body.description
			},
			{ where: { id: { [op.eq]: req.body.id } }}
			).then(() => {
				device.update(
					{ groupId: null },
					{ where: { groupId: { [op.eq]: req.body.id }  } 
				}).then(() => {

				    device.update(
				    	{ groupId: req.body.id },
						{ where: { id: { [op.in]: newGroupDevices } } }
					).then(() => {

						playlistGroup.destroy({
							where: { groupId: { [op.eq]: req.body.id } } 
						}).then(() => {

							playlistGroup.bulkCreate(newGroupPlaylists)
							.then(() => {
								req.session.mymessage = {type: "success", text: "Grupa ažurirana!"};
								res.redirect('/group/list');
							}).catch(error => {
								console.log("dogodila se greška pri ažuriranju popisa grupe!");
								console.log(error);
								req.session.mymessage = {type: "danger", text: "Dogodila se greška pri ažuriranju popisa grupe!"};
								res.redirect('/group/list');
							});

						}).catch(error => {
							console.log("dogodila se greška pri brisanju popisa grupe!");
							req.session.mymessage = {type: "danger", text: "Dogodila se greška pri brisanju popisa grupe!"};
							res.redirect('/group/list');
						});

					}).catch(error => {
						console.log("dogodila se greška pri ažuriranju uređaja grupe!");
						req.session.mymessage = {type: "danger", text: "Dogodila se greška pri ažuriranju uređaja grupe!"};
						res.redirect('/group/list');
					});

				}).catch(error => {
					console.log("dogodila se greška pri brisanju uređaja grupe!");
					console.log(error);
					req.session.mymessage = {type: "danger", text: "Dogodila se greška pri brisanju uređaja grupe!"};
					res.redirect('/group/list');
				});

			});

		}
		else{

			console.log("group create! ");

			group.create({  
				companyId: req.user.companyId,
				name: req.body.name,
		        description: req.body.description
			}).then(newGr => {	
				console.log("newGr created!");
			    console.log(newGr.get({plain: true}));

				for (var key in req.body) {
			    	if(key.startsWith("dev-") && req.body[key] == "on"){
			    		var devId = key.split('-')[1];
			    		newGroupDevices.push(parseInt(devId));
			    	}else if(key.startsWith("pl-") && req.body[key] == "on"){
			    		var plId = key.split('-')[1];
			    		newGroupPlaylists.push({groupId: newGr.id, playlistId: parseInt(plId)});
			    	}
			    }	
			    console.log("newGroupDevices");
			    console.log(newGroupDevices);
			    console.log("newGroupPlaylists");
			    console.log(newGroupPlaylists);

			    device.update(
					{ groupId: newGr.id },
					{ where: { id: { [op.in]: newGroupDevices }  } 
				}).then(() => {
			    	
					playlistGroup.bulkCreate(newGroupPlaylists)
					.then(() => {
						console.log("bulkCreate done!");
						req.session.mymessage = {type: "success", text: "Grupa kreirana!"};
						res.redirect('/group/list');
					}).catch(error => {
						console.log("dogodila se greška pri kreiranju popisa grupe!");
						console.log(error);
						req.session.mymessage = {type: "danger", text: "Dogodila se greška pri kreiranju popisa grupe!"};
						res.redirect('/group/list');
					});

				}).catch(error => {
					console.log("dogodila se greška pri kreiranju uređaja grupe!");
					req.session.mymessage = {type: "danger", text: "Dogodila se greška pri kreiranju uređaja grupe!"};
					res.redirect('/group/list');
				});

			}).catch(error => {
				console.log("dogodila se greška pri kreiranju grupe!");
				console.log(error);
				req.session.mymessage = {type: "danger", text: "Dogodila se greška pri kreiranju grupe!"};
				res.redirect('/group/list');
			});

		}

	}

};