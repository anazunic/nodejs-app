var models = require("../models");
const op = require("sequelize").Op;
var company = models.company;
var user = models.user;

module.exports = {

	list: (req, res) => {
		console.log("user list user:");
		user.findAll({ 
			where:{
				firstName: { [op.ne]: "admin"},
				email: {[op.or]: {
			      [op.ne]: "admin@gmail.com",
			      [op.eq]: null
			    }}
			},
			order: [ ['lastName', 'ASC'] ]
    	},
    	{raw: true })
    	.then(allUsers => {
			var flashMessage;
			if( req.session.mymessage ){
				flashMessage = req.session.mymessage;
				req.session.mymessage = null;
			}
			res.render('user/list', {
				user: req.user,
				allUsers: allUsers,
				message: flashMessage
			});

		}).catch(error => {
			console.log("Nije moguće pronaći korisnike!");
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći korisnike!"};
			res.redirect('/user/list');
		});
		
	},

	show: (req, res) => {
		console.log("user show");
		console.log(req.params);
		user.findById(req.params.id, {
			include: [{ model: company, as: 'company' }]
		}).then(u => {
			console.log(u.get({plain: true}));
			res.render('user/show', {
				user: req.user,
				u: u
			});
		}).catch(error => {
			console.log("Nije moguće pronaći korisnika!");
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći korisnika!"};
			res.redirect('/user/list');
		});
	},

	create: (req, res) => {
		company.findAll({ 
			where:{
				name: { [op.ne]: "admin company"},
				email: {[op.or]: {
			      [op.ne]: "admincompany@gmail.com",
			      [op.eq]: null
			    }}
			},
			order: [ ['name', 'ASC'] ]
    	},
    	{ raw: true })
    	.then(allCompanies => {
			res.render('user/create', {
				user: req.user,
				allCompanies: allCompanies
			});
		}).catch(error => {
			console.log("Nije moguće pronaći kompanije!");
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći kompanije!"};
			res.redirect('/user/list');
		});
	},

	edit: (req, res) => {
		console.log("inside edit!");
		console.log(req.params);
		user.findById(req.params.id, {
			include: [{ model: company, as: 'company' }]
		}).then(u => {
			company.findAll({ 
				where:{
					name: { [op.ne]: "admin company"},
					email: {[op.or]: {
				      [op.ne]: "admincompany@gmail.com",
				      [op.eq]: null
				    }}
				},
				order: [ ['name', 'ASC'] ]
	    	},
	    	{ raw: true })
	    	.then(allCompanies => {
				console.log(u.get({plain: true}));
				res.render('user/edit', {
					user: req.user,
					allCompanies: allCompanies,
					u: u
				});
			}).catch(error => {
				console.log("Nije moguće pronaći kompanije!");
				req.session.mymessage = {type: "danger", text: "Nije moguće pronaći kompanije!"};
				res.redirect('/user/list');
			});
			
		}).catch(error => {
			console.log("Nije moguće pronaći korisnika!");
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći korisnika!"};
			res.redirect('/user/list');
		});
		
	},

	save: (req, res) => {
		console.log("user save body params:");
		console.log(req.body);

		if(req.body.id){

			u.update({  
				firstName: req.body.firstName,
		        lastName: req.body.lastName,
		        about: req.body.about,
		        email: req.body.email ? req.body.email : null,
		        password: req.body.password ? user.generateHash(req.body.password) : u.password,
		        companyId: req.body.companyId
			},
			{ where: { id: req.body.id }}
			).then(updatedUser => {
				console.log("korisnik ažuriran!");
				console.log(updatedUser.get({plain: true}));

				req.session.mymessage = {type: "success", text: "Korisnik ažuriran!"};
				res.redirect('/user/list');

			}).catch(error => {
				console.log("dogodila se greška pri ažuriranju!");
				req.session.mymessage = {type: "danger", text: "Dogodila se greška pri ažuriranju!"};
				res.redirect('/user/list');
			});


		}else{

			user.findOne({ where: {email: req.body.email} }).then(existingUser => {
				if(existingUser){
					req.session.mymessage = {type: "danger", text: "Korisnik već postoji!"};
					res.redirect('/user/list');
				}else{


					user.create({  
						firstName: req.body.firstName,
				        lastName: req.body.lastName,
				        about: req.body.about,
				        email: req.body.email,
				        password: user.generateHash(req.body.password),
				        companyId: req.body.companyId
					}).then(newUser => {		
						console.log(newUser.get({plain: true}));
			            console.log("korisnik kreiran!");
			            req.session.mymessage = {type: "success", text: "Korisnik kreiran!"};
						res.redirect('/user/list');
					}).catch(error => {
						console.log("dogodila se greška pri kreiranju!");
						req.session.mymessage = {type: "danger", text: "Dogodila se greška pri kreiranju!"};
						res.redirect('/user/list');
					});

					
				}
			})

			

		}
	},

	

	delete: (req, res) => {
		console.log("inside delete!");
		console.log(req.params);
		user.findById(req.params.id).then(u => {

			/*TODO delete all videos, groups, devices, */

			u.destroy().then(() => {
				req.session.mymessage = {type: "success", text: "Korisnik uklonjena!"};
				res.redirect('/user/list');
			});
			
			
		}).catch(error => {
			req.session.mymessage = {type: "danger", text: "Nije moguće pronaći korisnika!"};
			res.redirect('/user/list');
		});
		
	}



};