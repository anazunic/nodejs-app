module.exports = (sequelize, Sequelize) => {
    var WifiSetup = sequelize.define('wifisetup', {
        id: { autoIncrement: true, primaryKey: true, type: Sequelize.BIGINT},
        name: { type: Sequelize.STRING, notEmpty: true},
        type: { type: Sequelize.STRING, notEmpty: true},
        password: { type: Sequelize.STRING}
    },
    {
        associate: (db) => {
            db.wifisetup.belongsTo(db.device);
        }
    });
    return WifiSetup;
};