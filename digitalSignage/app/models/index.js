"use strict";

var fs = require("fs"); //modul za kopanje po file sistemu
var path = require("path"); //modul za izradu adresa prema nekom fajlu
var Sequelize = require("sequelize"); //modul za pristup bazi podataka
var env = process.env.NODE_ENV || "development"; //odredis u kojem si environmentu

//preko require i path modula pročitaš config/database.js datoteku (ona vraća onaj JSON sa postavkama baze
//za različite environmente
//[env] uzmes postavke baze od odredenog environmenta, da si ima vise baza isa bi [dbName][env]
var databaseConfig = require(path.join(__dirname, '..', '..', 'config', 'database'))[env];

//sad se samo spojis na bazu preko podataka koje si iscita iz config datoteke
var sequelize = new Sequelize(databaseConfig.name, databaseConfig.username, databaseConfig.password, databaseConfig.options);
var db = {};

//Ova funkcija ti proleti kroz sve fajlove unutar trenutnog foldera i njihove module.exports ubaci u db JSON objekt pa da moš kasnije koristit db.user
fs.readdirSync(__dirname).filter(function(file) {
	return (file.indexOf(".") !== 0) && (file !== "index.js");
}).forEach(function(file) {
    var model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
});

//ovo ti pregleda imaš li u modelima kakvih asocijacija (veze 1:n ili n:m) pa ti odma napravi strane ključeve
Object.keys(db).forEach(function(modelName) {
    if ("associate" in db[modelName].options) {
        db[modelName].options.associate(db);
    }
});
//dodas sequelize objekt spojen na bazu u exports
db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;