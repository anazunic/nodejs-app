
module.exports = (sequelize, Sequelize) => {
    var Device = sequelize.define('device', {
        id: { autoIncrement: true, primaryKey: true, type: Sequelize.BIGINT},
        name: { type: Sequelize.STRING, notEmpty: true},
        place: { type: Sequelize.STRING },
        macaddress: {type: Sequelize.STRING(17), notEmpty: true}, //01:23:45:67:89:AB
        description: {type: Sequelize.STRING}
    },
    {
        associate: (db) => {
            db.device.belongsTo(db.company);
            db.device.belongsTo(db.group);
        }
    });
    return Device;
};
