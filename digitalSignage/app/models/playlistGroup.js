
module.exports = (sequelize, Sequelize) => {
    var PlaylistGroup = sequelize.define('playlistGroup', {
        id: { autoIncrement: true, primaryKey: true, type: Sequelize.BIGINT}
    },
    {
    	associate: (db) => {
    		db.playlistGroup.belongsTo(db.group, {as: 'groupItem', foreignKey: 'groupId'});
    		db.playlistGroup.belongsTo(db.playlist, {as: 'playlistItem', foreignKey: 'playlistId'});
    	}
    });
    return PlaylistGroup;
};