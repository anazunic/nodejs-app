
module.exports = (sequelize, Sequelize) => {
    var PlaylistVideo = sequelize.define('playlistVideo', {
    	id: { autoIncrement: true, primaryKey: true, type: Sequelize.BIGINT},
        position: { type: Sequelize.INTEGER }
    },{
    	associate: (db) => {
    		db.playlistVideo.belongsTo(db.video, {as: 'videoItem', foreignKey: 'videoId'});
    		db.playlistVideo.belongsTo(db.playlist, {as: 'playlistItem', foreignKey: 'playlistId'});
    	}
    });
    return PlaylistVideo;
};