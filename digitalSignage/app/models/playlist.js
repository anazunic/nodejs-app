
module.exports = (sequelize, Sequelize) => {
    var Playlist = sequelize.define('playlist', {
        id: { autoIncrement: true, primaryKey: true, type: Sequelize.BIGINT},
        name: { type: Sequelize.STRING, notEmpty: true},
        description: { type: Sequelize.STRING },
        startTime: { type: Sequelize.DATE}
    },
    {
        associate: (db) => {
          
          db.playlist.belongsTo(db.company);
          
          db.playlist.belongsToMany(db.group, { through: { 
              model: db.playlistGroup, 
              foreignKey: 'playlistId', 
              as: "groups",
              unique: false }
            });

          db.playlist.belongsToMany(db.video, 
            { through: { 
              model: db.playlistVideo, 
              foreignKey: 'playlistId', 
              as: "videos", 
              unique: false }
            });
        }
    });
    return Playlist;
};