module.exports = (sequelize, Sequelize) => {
    var Company = sequelize.define('company', {
        id: { autoIncrement: true, primaryKey: true, type: Sequelize.BIGINT},
        name: { type: Sequelize.STRING, notEmpty: true},
        address: { type: Sequelize.STRING, notEmpty: true},
        oib: {type: Sequelize.STRING(9), notEmpty: true},
        phone: {type: Sequelize.STRING},
        email: {type:Sequelize.STRING, 
            notEmpty: false, 
            validate: {
                len: {
                    args: [6, 128],
                    msg: "Email address must be between 6 and 128 characters in length"
                },
                isEmail: {
                    msg: "Email address must be valid"
                }
            }}
        });
    return Company;
};
