module.exports = (sequelize, Sequelize) => {
    var Log = sequelize.define('log', {
        id: { autoIncrement: true, primaryKey: true, type: Sequelize.BIGINT},
        logstring: { type: Sequelize.STRING, notEmpty: true},
        filterflag: { type: Sequelize.BOOLEAN }
    },
    {
        associate: (db) => {
            db.log.belongsTo(db.device);
        }
    });
    return Log;
};