
module.exports = (sequelize, Sequelize) => {
    var Group = sequelize.define('group', {
        id: { autoIncrement: true, primaryKey: true, type: Sequelize.BIGINT},
        name: { type: Sequelize.STRING, notEmpty: true},
        description: { type: Sequelize.STRING}
    },
    {
        associate: (db) => {
        	db.group.belongsTo(db.company);
        	//db.group.belongsToMany(db.playlist,     { through: 'playlistGroup', foreignKey: 'groupId' });
            db.group.belongsToMany(db.playlist, { through: { 
              model: db.playlistGroup, 
              unique: false,
              foreignKey: 'groupId', 
              as: "playlists"}
            });

            
        }
    });
    return Group;
};