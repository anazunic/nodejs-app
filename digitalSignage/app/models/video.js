module.exports = (sequelize, Sequelize) => {
    var Video = sequelize.define('video', {
        id: { autoIncrement: true, primaryKey: true, type: Sequelize.BIGINT},
        name: { type: Sequelize.STRING, notEmpty: true},
        duration: { type: Sequelize.INTEGER, notEmpty: true},
        path: {type: Sequelize.TEXT},
        format : {type: Sequelize.STRING},
        size: {type: Sequelize.FLOAT, notEmpty: true},
        path: {type: Sequelize.TEXT}
    },
    {
        associate: (db) => {
            db.video.belongsTo(db.company);
            //db.video.belongsToMany(db.playlist, { through: { model: db.playlistVideo, as: "videoId", unique: false}});
            db.video.belongsToMany(db.playlist, 
                { through: { 
                  model: db.playlistVideo, 
                  foreignKey: 'videoId', 
                  as: "playlists", 
                  unique: false}
            });
        }
    });
    return Video;
};