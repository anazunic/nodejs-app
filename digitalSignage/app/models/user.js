var bcrypt   = require('bcrypt-nodejs');

module.exports = (sequelize, Sequelize) => {
    var User = sequelize.define('user', {
        id: { autoIncrement: true, primaryKey: true, type: Sequelize.BIGINT},
        firstName: { type: Sequelize.STRING, notEmpty: true},
        lastName: { type: Sequelize.STRING, notEmpty: true},
        about : {type: Sequelize.TEXT},
        email: { type:Sequelize.STRING },
        password : {type: Sequelize.STRING, allowNull: false },
    },
    {
        associate: (db) => {
            db.user.belongsTo(db.company);
        }
    });

    User.generateHash = (password) => {
        return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    };

    return User;
};