var bCrypt = require("bcrypt-nodejs");
var models = require("../app/models");
var LocalStrategy = require("passport-local").Strategy;
module.exports = function(passport, user){

    var User = user;

    passport.serializeUser(function(user, done) {
        console.log("serializeUser");
        var myUser = {
            id: user.id,
            companyId: user.companyId,
            username: user.username,
            firstName: user.firstName,
            lastName: user.lastName,
            isAdmin: user.isAdmin,
        };
        done(null, myUser);
    });

    passport.deserializeUser(function(user, done) {
        console.log("deserializeUser");
        User.findById(user.id).then(function(user) {
            if(user){
                var myUser = user.get();
                myUser.isAdmin = (myUser.email == "admin@gmail.com");
                done(null, myUser);
            }
            else{
                done(user.errors, null);
            }
        });
    });
    

    //LOCAL SIGNIN
    passport.use("local-signin", new LocalStrategy({
            usernameField : "email", // by default, local strategy uses username and password, we will override with email
            passwordField : "password",
            passReqToCallback : true // allows us to pass back the entire request to the callback
        }, function(req, email, password, done) {

            var User = user;
            var isValidPassword = function(userpass,password){ 
                return bCrypt.compareSync(password, userpass); 
            };

            User.findOne({ where : { email: email}}).then(function (user) {

                if (!user) {
                    return done(null, false, req.flash("loginMessage", "Email adresa nije pronađena!"));
                }

                if (!isValidPassword(user.password, password)) {
                    return done(null, false, req.flash("loginMessage", "Pogrešan password!"));
                }

                var userInfo = user.get();
                userInfo.isAdmin = (userInfo.email == "admin@gmail.com");
                
                return done(null, userInfo);

            }).catch(function(err){
                return done(null, false, { message: "Dogodila se greška!" });
            });
        }
    ));


    
};

