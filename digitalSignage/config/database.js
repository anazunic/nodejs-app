module.exports = {
    development: {
            "name": "digitalSignageDb",
            "username": "root",
            "password": "admin",
            "options": {
                "timezone":"Europe/Zagreb",
                "host": "localhost",
                "dialect": "mysql",
                "pool": {
                  "max": 5,
                  "min": 0,
                  "acquire": 30000,
                  "idle": 10000
                } 
            }
    },
    test: {},
    production: {}
}